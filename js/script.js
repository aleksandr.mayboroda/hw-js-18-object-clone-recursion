// null, function ... конечно же это сломают, но по условию я не заморачивался

objectCloneStart({
    aaa:123,
    users: [
        {name: 'aaa'},
        {name: 'bbb'},
        {name: 'ccc'},
        {lol: [1,2,3,4,5]},
    ],
    obb: {
        ddd: 1,
        eee: 2,
    }

})

function objectCloneStart(object) {
    let newObj = {}
    for(let prop in object)
    {
        newObj[prop] = myClone(object[prop])
    }

    console.log('result',newObj)
}

function myClone(params) {
    if(Array.isArray(params))
    {
        return params.map(param => myClone(param))
    }
    else if(typeof params === 'object')
    {
        let newObj = {}
        for(let prop in params)
        {
            newObj[prop] = myClone(params[prop])
        }
        return newObj
    }
    else
    {
        return params
    }
}